import pytest
from python.RandomForest import RandomForestClassifier
import pandas as pd
from sklearn.model_selection import train_test_split
from python import CSVReader
from collections import Counter

@pytest.fixture(scope="session")
def read_value():
    df_value = pd.read_csv("../data/income.csv", usecols=[13], names=["values"])
    return df_value


@pytest.fixture(scope="session")
def read_source():
    data_source = CSVReader.read_csv("../data/income.csv")
    return data_source


@pytest.fixture(scope="session")
def trained_model():
    rf = RandomForestClassifier.perform_rf()
    return rf


@pytest.mark.run(order=1)
def test_case_train_data_partition_validation(trained_model, read_source):
    train_set_record_count = len(RandomForestClassifier.train)
    expected_record_count = (len(read_source))*.7

    assert int(train_set_record_count) >= int(expected_record_count)  # Expects training data set to be at least 70%

# check if training data set target are balanced
@pytest.mark.run(order=2)
def test_case_check_train_data_set(read_value, read_source, trained_model):

    prediction_list = []
    train_data = RandomForestClassifier.train  # Trained data set used by function
    total_sample = (len(train_data))

    features = [ft[:-1] for ft in train_data]
    values = [ft[-1] for ft in train_data]

    for feature, value in zip(features, values):
        prediction = trained_model.predict(feature)
        prediction_list.append(prediction)

    # Expects predicted percentage on both outcome closer to 25%

    prediction_list = dict(Counter(prediction_list))

    targeted_outcome_1 = (prediction_list.get("<=50K")/total_sample)*100
    targeted_outcome_2 = (prediction_list.get(">50K")/total_sample)*100

    assert abs(targeted_outcome_1-targeted_outcome_2) <= 25

